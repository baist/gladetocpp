
use crate::types::*;

static BOOL_PROPS: &'static [&str] = &[
    "activates_default",
    "active",
    "always_show_image",
    "app_paintable",
    "can_default",
    "can_focus",
    "column_homogeneous",
    "draw_indicator",
    "double_buffered",
    "has_default",
    "has_focus",
    "hexpand",
    "homogeneous",
    "is_focus",
    "overwrite_mode",
    "receives_default",
    "row_homogeneous",
    "sensitive",
    "vexpand",
    "visible"
    ];

static INT_PROPS: &'static [&str]  = &[
    "border_width",
    "width_chars",
    "width_request",
    "height_request",
    "spacing",
    "margin_left",
    "margin_right",
    "margin_top",
    "margin_bottom"
    ];

static ENUM_PROPS: &'static [(&str, &str)]  = &[
    ("shadow_type", "Gtk::SHADOW"),
    // TODO: uppercase doesn't work here
    ("input_hints", "Gtk::INPUT_HINT"),
    ("input_purpose", "Gtk::INPUT_PURPOSE"),
    ("justify", "Gtk::JUSTIFY"),
    ("layout_style", "Gtk::BUTTONBOX"),
    ("halign", "Gtk::ALIGN"),
    ("valign", "Gtk::ALIGN"),
    ("orientation", "Gtk::ORIENTATION"),
    ("baseline_position", "Gtk::BASELINE_POSITION"),
    ];

static STR_PROPS: &'static [&str] = &[
    "label",
    "primary_icon_tooltip_text",
    "secondary_icon_tooltip_text",
    "placeholder_text",
    "text"
    ];


// GtkWindow -> Gtk::Window
fn gen_gtkmm_type(tp: &String) -> String
{
    // TODO: не стандартные случаи
    tp.replace("Gtk", "Gtk::")
}

fn gen_gtkmm_property(key: &String, val: &String) -> String
{
    use std::collections::HashMap;
    let enums: HashMap<&str, &str> = ENUM_PROPS.iter().cloned().collect();

    let mut res: String = String::new();
    if BOOL_PROPS.contains(&key.as_str()) {
        if val == "True" {
            res = format!("property_{}() = true", key);
        }
        else {
            res = format!("property_{}() = false", key);
        }
    }
    else if INT_PROPS.contains(&key.as_str()) {
        res = format!("property_{}() = {}", key, val);
    }
    else if STR_PROPS.contains(&key.as_str()) {
        res = format!("property_{}() = \"{}\"", key, val);
    }
    else if enums.contains_key(&key.as_str()) {
        let prefix = enums.get(&key.as_str()).unwrap();
        res = format!("property_{}() = {}_{}", 
            key, prefix, val.to_uppercase());
    }
    else if let Ok(r) = val.parse::<i32>() {
        res = format!("property_{}() = {}", key, r);
    }
    else if let Ok(r) = val.parse::<f64>() {
        res = format!("property_{}() = {}", key, r);
    }
    // else if val.parse::<f32>().is_ok() { }
    else {
        // строка по умолчанию
        res = format!("property_{}() = \"{}\"", key, val);
    }

    res
}

fn gen_gtkmm_style(st: &String) -> String
{
    format!("get_style_context()->add_class(\"{}\")", st)
}

fn gen_gtkmm_packing(pckng: &Vec<Property>, child: &String) -> String
{
    if pckng.is_empty() {
        // return "".to_string();
        return format!("add({})", child);
    }

    let check_prop = pckng.first().unwrap();
    if check_prop.key == "x" || check_prop.key == "y" {
        let mut x = 0;
        let mut y = 0;
        for p in pckng {
            if p.key == "x" {
                x = p.val.parse::<i32>().unwrap();
            }
            else if p.key == "y" {
                y = p.val.parse::<i32>().unwrap();
            }
        }
        return format!("put({}, {}, {})", child, x, y);
    }
    else if check_prop.key.contains("_attach") {
        let mut left = 0;
        let mut top = 0;
        let mut width = 1;
        let mut height = 1;
        for p in pckng {
            if p.key == "left_attach" {
                left = p.val.parse::<i32>().unwrap();
            }
            else if p.key == "top_attach" {
                top = p.val.parse::<i32>().unwrap();
            }
            else if p.key == "width" {
                width = p.val.parse::<i32>().unwrap();
            }
            else if p.key == "height" {
                height = p.val.parse::<i32>().unwrap();
            }
        }
        return format!("attach({}, {}, {}, {}, {})",
            child, left, top, width, height);
    }
    // TODO: а если не первый?
    else if check_prop.key == "expand" {
        let mut expand = false;
        let mut fill = true;
        let mut pack_type = "pack_start";
        for p in pckng {
            if p.key == "expand" {
                expand = (p.val == "True");
            }
            else if p.key == "fill" {
                fill = (p.val == "True");
            }
            else if p.key == "pack_type" {
                if p.val == "end" {
                    pack_type = "pack_end";
                }
            }
        }
        return format!("{}({}, {}, {})",
            pack_type, child, expand.to_string(), fill.to_string());
    }
    format!("add({})", child)
}

fn members_to_cpp(members: &Vec<(String, String)>) -> String
{
    let mut res: String = String::new();
    for (a, b) in members {
        let s = format!("        {} {};\n", gen_gtkmm_type(&a), b);
        res.push_str(&s);
    }
    res
}

fn styles_to_cpp(wdgt: &Widget) -> String
{
    let mut res: String = String::new();
    for style in &wdgt.styles {
        let s = format!("{}{}.{};\n", 
            "            ", wdgt.name, gen_gtkmm_style(style));
        res.push_str(&s);
    }
    res
}

fn props_to_cpp(wdgt: &Widget) -> String
{
    let mut res: String = String::new();
    for prop in &wdgt.props {
        let a = gen_gtkmm_property(&prop.key, &prop.val);
        let s = format!("{}{}.{};\n", "            ", wdgt.name, a);
        res.push_str(&s);
    }
    res
}

fn root_to_cpp(root: &Widget) -> String
{
    let mut members: Vec<(String, String)> = Vec::new();
    let mut body: String = String::new();
    let mut stack: Vec<&Widget> = Vec::new();

    {
        {
            for style in &root.styles {
                let s = format!("{}p_{}->{};\n", 
                    "            ", root.name, gen_gtkmm_style(style));
                body.push_str(&s);
            }
            for prop in &root.props {
                let a = gen_gtkmm_property(&prop.key, &prop.val);
                let s = format!("{}p_{}->{};\n", "            ", root.name, a);
                body.push_str(&s);
            }
            body.push_str("\n");
        }
        for w in &root.children {
            members.push( (w.type_.clone(), w.name.clone()) );
            body.push_str(&styles_to_cpp(w));
            body.push_str(&props_to_cpp(w));
            {
                let s = format!("{}p_{}->{};\n", 
                    "            ", root.name, gen_gtkmm_packing(&w.packing, &w.name));
                body.push_str(&s);
            }
            body.push_str("\n");
            stack.push(&w);
        }
    }

    while let Some(parent) = stack.pop() {
        for w in &parent.children {
            members.push( (w.type_.clone(), w.name.clone()) );
            body.push_str(&styles_to_cpp(w));
            body.push_str(&props_to_cpp(w));
            {
                let s = format!("{}{}.{};\n", 
                    "            ", parent.name, gen_gtkmm_packing(&w.packing, &w.name));
                body.push_str(&s);
            }
            body.push_str("\n");
            stack.push(&w);
        }
    }

    let res: String = "\
    class @root_name@
    {
    public:
@member_list@

    public:
        @root_name@() {}
        ~@root_name@() {}

        void setupUi(@root_type@ * p_@root_name@)
        {
@body@
        }
    };".to_string();

    res.
        replace("@root_name@", &root.name).
        replace("@root_type@", &gen_gtkmm_type(&root.type_) ).
        replace("@member_list@", &members_to_cpp(&members) ).
        replace("@body@", &body)
}

pub fn to_cpp(wdgts: &Vec<Widget>, filename: &String) -> String
{
    //let tempStr: &str = 

    let mut classes: String = String::new();

    for w in wdgts {
        classes.push_str( &root_to_cpp(&w) )
    }

    let res: String = "\
// Form for Gtkmm 3.0 generated by gladetocpp from Glade file '@source_file@'

// WARNING! All changes made in this file will be lost when recompiling UI file!

#pragma once

#include <gtkmm.h>

namespace Ui
{
    @root_objects@
}".to_string();

    res.replace("@root_objects@", &classes).
        replace("@source_file@", filename)
}