

#[derive(Debug)]
pub struct Property
{
    pub key: String,
    pub val: String
}

#[derive(Debug)]
pub struct Packing
{
    pub name: String,
    pub val: String
}

#[derive(Debug)]
pub struct Child
{
    pub wdgt: Widget,
    pub packing: Vec<Property>
}

#[derive(Debug)]
pub struct Widget
{
    pub name: String,
    pub type_: String,
    pub props: Vec<Property>,
    pub styles: Vec<String>,
    pub packing: Vec<Property>,
    pub children: Vec<Widget>
}

impl Widget
{
    pub fn new() -> Self
    {
        Self {
            name: "_empty_".to_string(),
            type_: "".to_string(),
            props: Vec::new(),
            styles: Vec::new(),
            packing: Vec::new(),
            children: Vec::new()
        }
    }
}