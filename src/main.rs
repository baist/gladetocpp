use std::env;
use std::rc::Rc;
use std::collections::HashSet;

mod types;
use types::*;

mod reader;
use reader::*;

mod to_cpp;
use to_cpp::*;

fn main()
{
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("no file");
        return;
    }

    let wdgts = read_file(&args[1]);

    println!("{}", to_cpp(&wdgts, &args[1]));
}

