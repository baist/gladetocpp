use std::fs::File;
use std::io::BufReader;

use xmltree::Element;

use crate::types::*;

static mut ID_COUNT: u32 = 0;

pub fn read_file(path: &str) -> Vec<Widget>
{
	let file = File::open(path).unwrap();
    let mut buf_reader = BufReader::new(file);
    let mut names_element = Element::parse(buf_reader).unwrap();
    proc_interface(&names_element)
}

fn proc_interface(el: &Element) -> Vec<Widget>
{
    let mut res: Vec<Widget> = Vec::new();
    for e in &el.children {
        if e.name == "object" {
            if let Some(wdgt) = proc_object(e) {
                res.push(wdgt);
            }
        }
    }

    return res;
}

fn proc_root(el: &Element)
{
}

fn proc_object(el: &Element) -> Option<Widget>
{
    let mut type_ = String::new();

    if let Some(r) = el.attributes.get("class") {
        type_ = r.to_string();
    }
    else {
        println!("unknown type");
        return None;
    }

    if type_ == "GtkSizeGroup" {
        return None;
    }
    else {
        return proc_widget(el);
    }

    return None;
}

fn proc_widget(el: &Element) -> Option<Widget>
{
    let mut wdgt = Widget::new();

    if let Some(r) = el.attributes.get("class") {
        wdgt.type_ = r.to_string();
    }
    else {
        println!("expected widget type");
        return None;
    }

    if let Some(r) = el.attributes.get("id") {
        wdgt.name = r.to_string();
    }
    else {
        let mut id_count;
        unsafe {
            id_count = ID_COUNT;
            ID_COUNT += 1;
        }
        wdgt.name = format!("id_{}_{}", wdgt.type_, id_count);
    }

    wdgt.props = proc_props(el);

    if let Some(r) = el.get_child("style") {
        wdgt.styles = proc_styles(r);
    }

    wdgt.children = proc_children(el);

    Some(wdgt)
}

fn proc_props(el: &Element) -> Vec<Property>
{
    let mut res = Vec::new();

    for e in &el.children {
        if e.name == "property" {
            if let Some(k) = e.attributes.get("name") {
                if let Some(v) = &e.text {
                    res.push( Property { key: k.to_string(), val: v.to_string() } );
                }
            }
        }
    }

    res
}

fn proc_styles(el: &Element) -> Vec<String>
{
    let mut res = Vec::new();

    for e in &el.children {
        if e.name == "class" {
            if let Some(k) = e.attributes.get("name") {
                res.push( k.to_string() );
            }
        }
    }

    res
}

fn proc_packing(el: &Element) -> Vec<Property>
{
    return proc_props(el);
}

fn proc_children(el: &Element) -> Vec<Widget>
{
    let mut res = Vec::new();

    for e in &el.children {
        if e.name == "child" {
            if let Some(r) = proc_child(e) {
                res.push(r);
            }
        }
    }

    res
}

fn proc_child(el: &Element) -> Option<Widget>
{
    if let Some(e) = el.get_child("object") {
        let res_wdgt = proc_object(e);
        if res_wdgt.is_none() {
            return None;
        }
        let mut wdgt = res_wdgt.unwrap();
        if let Some(r) = el.get_child("packing") {
            wdgt.packing = proc_packing(r);
        }
        return Some(wdgt);
    }
    else {
        return None;
    }

    None
}